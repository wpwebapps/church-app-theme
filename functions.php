<?php

// Remove all default WP template redirects/lookups
    remove_action( 'template_redirect', 'redirect_canonical' );

// add support for features
    function church_app_theme_add_theme_support() {

        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'church-core-overrides' );

    }
    add_action( 'after_setup_theme', 'church_app_theme_add_theme_support' );


// load Kirki
    if ( ! class_exists( 'Kirki' ) ) {
        include( get_template_directory().'/includes/kirki/kirki.php' ); // class-kirki-modules-css.php line 141
    }

// load form builder    
    include( get_template_directory().'/includes/form.php' );


// load template loader for church core
   // include( get_template_directory().'/includes/church-core.php' );

// load advanced custom fields pro
    if ( ! class_exists( 'ACF' ) ) {
        // modified class-acf-field-image.php to return empty instead of false when no value
        include( get_template_directory().'/includes/acf.php' );
    }

// load required files
    function redeemer_church_required_files() {

        include( get_template_directory().'/includes/customizer.php' );
        include( get_template_directory().'/includes/media.php' );
        include( get_template_directory().'/includes/scripts.php' );
        include( get_template_directory().'/includes/gravity-forms.php' );
        include( get_template_directory().'/includes/endpoints/helpers.php' );
        include( get_template_directory().'/includes/endpoints/mailchimp.php' );
        include( get_template_directory().'/includes/endpoints/options.php' );
        include( get_template_directory().'/includes/endpoints/page.php' );
        include( get_template_directory().'/includes/endpoints/events.php' );

    }
    add_action( 'init', 'redeemer_church_required_files' );


// register navigation menu
    function church_core_app_navigation_menu() {
        register_nav_menus(
            array(
                'app-main' => __( 'Main Menu', 'textdomain' )
            )
        );
    }
    add_action( 'init', 'church_core_app_navigation_menu' );


// custom excerpt more tag
    function church_app_theme_new_excerpt_more( $more ) {
        return '<a class="more-link" href="' . get_permalink() . '">...more</a>';
    }
    add_filter( 'excerpt_more', 'church_app_theme_new_excerpt_more' );


// Allow cross domain requests
function church_app_theme_cors_http_header(){

    header("Access-Control-Allow-Origin: *");

}
add_action( 'init', 'church_app_theme_cors_http_header' );