<?php

// customize ACF path
    function church_app_themes_acf_settings_path( $path ) {
        $path = get_stylesheet_directory() . '/includes/acf/';
        return $path;
    }
    add_filter('acf/settings/path', 'church_app_themes_acf_settings_path');
 

// customize ACF dir
    function church_app_themes_acf_settings_dir( $dir ) {
        $dir = get_stylesheet_directory_uri() . '/includes/acf/';
        return $dir;
    }
    add_filter('acf/settings/dir', 'church_app_themes_acf_settings_dir');
 

// Hide ACF field group menu item
    add_filter('acf/settings/show_admin', '__return_false');

//Include the /acf folder in the places to look for ACF Local JSON files
    add_filter('acf/settings/save_json', function() {
        return dirname(__FILE__) . '/acf-json';
    });
    add_filter('acf/settings/load_json', function() {
        $paths[] = dirname(__FILE__) . '/acf-json';
        return $paths;
    });


    include( get_template_directory().'/includes/acf/acf.php' );


// set text domain for translation of acf fields
    function church_app_theme_acf_translation() {
	
        acf_update_setting( 'l10n_textdomain', 'text-domain' );
        
    }
    add_action('acf/init', 'church_app_theme_acf_translation');

