<?php

function church_app_theme_church_core_template_overrides( $template ) {
    
    $root = get_template_directory().'/index.php';

	if ( is_single() && get_post_type() == 'podcast' ) {
        return $root;
    } else if ( is_post_type_archive( 'podcast' ) ) {
		return $root;
    } else if ( is_tax( 'series' ) ) {
        return $root;
    } else if ( is_tax( 'speaker' ) ) {
        return $root;
    } else if ( is_tax( 'podcast-tag' ) ) {
        return $root;
    } 

}
//add_filter( 'template_include', 'church_app_theme_church_core_template_overrides' );

function church_app_theme_core_overrides( $single_template ) {

    if ( is_single() && get_post_type() == 'podcast' ) {
        return get_template_directory().'/index.php';
    }

}
add_filter( 'single_template', 'get_custom_post_type_template', 999,1 );