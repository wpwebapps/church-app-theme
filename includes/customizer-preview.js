(function($) {
  // Update the site title in real time...
  wp.customize("header_logo", function(value) {
    value.bind(function(newval) {
      $("#header-logo").attr("src", newval);
    });
  });

  wp.customize("footer_logo", function(value) {
    value.bind(function(newval) {
      $("#footer-logo").attr("src", newval);
    });
  });

  wp.customize("mc_title", function(value) {
    value.bind(function(newval) {
      $(".mailchimp-intro .title").html(newval);
    });
  });

  wp.customize("mc_subtitle", function(value) {
    value.bind(function(newval) {
      $(".mailchimp-intro .subtitle").html(newval);
    });
  });

  wp.customize("mc_button_text", function(value) {
    value.bind(function(newval) {
      $(".mailchimp-form .button").html(newval);
    });
  });

  wp.customize("mc_placeholder", function(value) {
    value.bind(function(newval) {
      $(".mailchimp-form #email").attr("placeholder", newval);
    });
  });

  wp.customize("mc_confirmation", function(value) {
    value.bind(function(newval) {
      $(".mailchimp-response.success-msg").html(newval);
    });
  });

  wp.customize("notification_content", function(value) {
    value.bind(function(newval) {
      $(".notification-content").html(newval);
    });
  });

  wp.customize("footer_content", function(value) {
    value.bind(function(newval) {
      $(".footer-content").html(newval);
    });
  });
})(jQuery);
