<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Remove some default customizer options
function wpwa_church_app_remove_customizer_defaults() {     
	global $wp_customize;

	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' ); 
	$wp_customize->remove_section( 'title_tagline' ); 

	$wp_customize->remove_control( 'site_icon' );
	$wp_customize->remove_control( 'blogdescription' );

} 
add_action( 'customize_register', 'wpwa_church_app_remove_customizer_defaults', 11 );


// Do not proceed if Kirki does not exist.
if ( ! class_exists( 'Kirki' ) ) {
	return;
}

// configuration
Kirki::add_config(
	'church_app_theme', array(
		'capability'        => 'edit_theme_options',
		'option_type'       => 'theme_mod',
	)
);

// logos
Kirki::add_section( 'church_app_logos', array(
    'title'          => esc_attr__( 'Logos', 'textdomain' ),
    'priority'       => 10,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'image',
	'settings'    => 'header_logo',
	'label'       => esc_attr__( 'Header Logo', 'textdomain' ),
	'section'     => 'church_app_logos',
	'transport'   => 'postMessage',
	'default'     => get_template_directory_uri().'/images/redeemer-church.png',
	'priority'    => 10,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'image',
	'settings'    => 'footer_logo',
	'label'       => esc_attr__( 'Footer Logo', 'textdomain' ),
	'section'     => 'church_app_logos',
	'transport'   => 'postMessage',
	'default'     => get_template_directory_uri().'/images/redeemer-church.png',
    'priority'    => 20,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'image',
	'settings'    => 'loading_logo',
	'label'       => esc_attr__( 'Loading Screen Logo', 'textdomain' ),
	'section'     => 'church_app_logos',
	'transport'   => 'postMessage',
	'default'     => get_template_directory_uri().'/images/redeemer-church.png',
    'priority'    => 20,
) );


// header
Kirki::add_section( 'church_app_header', array(
    'title'          => esc_attr__( 'Header', 'textdomain' ),
    'priority'       => 20,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'header_links',
	'label'       => esc_attr__( 'Navigation Items', 'textdomain' ),
	'section'     => 'church_app_header',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '1em',
		'color'          => '#4a4a4a',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => '.navbar-menu a'
		),array(
			'element' => '.navbar-menu a:visited'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'header_links_hover',
	'label'       => __( 'Navigation Items (Hover)', 'textdomain' ),
	'section'     => 'church_app_header',
	'default'     => '#3273dc',
	'priority'    => 20,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output'      => array(
		array(
			'element' => '.navbar-menu a:hover',
			'property' => 'color'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'header_background',
	'label'       => __( 'Background', 'textdomain' ),
	'section'     => 'church_app_header',
	'default'     => '#ffffff',
	'priority'    => 30,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output'      => array(
		array(
			'element' => '.navbar',
			'property' => 'background-color'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'toggle',
	'settings'    => 'notification_search',
	'label'       => esc_attr__( 'Include Search', 'textdomain' ),
	'section'     => 'church_app_header',
	'default'     => '0',
	'priority'    => 50,
	'transport'   => 'auto',
	'choices'     => array(
		'on'  => esc_attr__( 'Yes', 'textdomain' ),
		'off' => esc_attr__( 'No', 'textdomain' ),
	),
) );

// background
Kirki::add_section( 'church_app_background', array(
    'title'          => esc_attr__( 'Background', 'textdomain' ),
    'priority'       => 25,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'background',
	'settings'    => 'body_background',
	'label'       => esc_attr__( 'Background', 'textdomain' ),
	'section'     => 'church_app_background',
	'transport'   => 'auto',
	'priority'    => 10,
	'default'     => array(
		'background-color'      => '#f2f2f2',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'output'      => array(
		array(
			'element' => 'body',
		),
	),
) );



// typography
Kirki::add_panel( 'church_app_typography', array(
    'priority'    => 30,
    'title'       => esc_attr__( 'Typography', 'textdomain' ),
    'description' => esc_attr__( 'Customize how text looks throughout your site.', 'textdomain' ),
) );

Kirki::add_section( 'church_app_typography_body', array(
    'title'          => esc_attr__( 'Body', 'textdomain' ),
	'description'    => esc_attr__( 'This is the main typography used throughout the site', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 5,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_body',
	'label'       => esc_attr__( 'Body', 'textdomain' ),
	'section'     => 'church_app_typography_body',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '1em',
		'color'          => '#121212',
	),
	'priority'    => 5,
	'output'      => array(
		array(
			'element' => 'body, p',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading1', array(
    'title'          => esc_attr__( 'H1', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 10,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h1',
	'label'       => esc_attr__( 'H1', 'textdomain' ),
	'section'     => 'church_app_typography_heading1',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '3em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h1, .title.is-1, .content h1',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading2', array(
    'title'          => esc_attr__( 'H2', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 20,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h2',
	'label'       => esc_attr__( 'H2', 'textdomain' ),
	'section'     => 'church_app_typography_heading2',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '2.5em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h2, .title.is-2, .content h2',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading3', array(
    'title'          => esc_attr__( 'H3', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 30,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h3',
	'label'       => esc_attr__( 'H3', 'textdomain' ),
	'section'     => 'church_app_typography_heading3',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '2.25em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h3, .title.is-3, .content h3',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading4', array(
    'title'          => esc_attr__( 'H4', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 40,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h4',
	'label'       => esc_attr__( 'H4', 'textdomain' ),
	'section'     => 'church_app_typography_heading4',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '2.0em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h4, .title.is-4, .content h4',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading5', array(
    'title'          => esc_attr__( 'H5', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 50,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h5',
	'label'       => esc_attr__( 'H5', 'textdomain' ),
	'section'     => 'church_app_typography_heading5',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '1.5em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h5, .title.is-5, .content h5',
		),
	),
) );

Kirki::add_section( 'church_app_typography_heading6', array(
    'title'          => esc_attr__( 'H6', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 60,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'typography',
	'settings'    => 'typography_h6',
	'label'       => esc_attr__( 'H6', 'textdomain' ),
	'section'     => 'church_app_typography_heading6',
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '1.25em',
		'color'          => '#363636',
		'text-transform' => 'uppercase',
	),
	'priority'    => 10,
	'output'      => array(
		array(
			'element' => 'h6, .title.is-6, .content h6',
		),
	),
) );

Kirki::add_section( 'church_app_typography_links', array(
    'title'          => esc_attr__( 'Links', 'textdomain' ),
	'panel'          => 'church_app_typography',
    'priority'       => 70,
) );


Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'link_color',
	'label'       => __( 'Link Color', 'textdomain' ),
	'section'     => 'church_app_typography_links',
	'default'     => '#3273dc',
	'transport'   => 'auto',
	'priority'    => 10,
	'choices'     => array(
		'alpha' => false,
	),
	'output'      => array(
		array(
			'element' => 'a, a:visited',
			'property' => 'color'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'link_color_hover',
	'label'       => __( 'Link Color (hover)', 'textdomain' ),
	'section'     => 'church_app_typography_links',
	'default'     => '#000000',
	'transport'   => 'auto',
	'priority'    => 20,
	'choices'     => array(
		'alpha' => false,
	),
	'output'      => array(
		array(
			'element' => 'a:hover',
			'property' => 'color'
		),
	),
) );




// MailChimp Options

Kirki::add_section( 'church_app_mailchimp', array(
    'title'          => esc_attr__( 'MailChimp', 'textdomain' ),
	'description'    => esc_attr__( 'Set options for your MailChimp subscription section.', 'textdomain' ),
    'priority'       => 60,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'toggle',
	'settings'    => 'mc_include',
	'label'       => esc_attr__( 'Include MailChimp section?', 'textdomain' ),
	'section'     => 'church_app_mailchimp',
	'description' => esc_attr__( 'Enabling/disabling requires publishing before changes can be seen in the customizer.', 'textdomain' ),
	'default'     => '0',
	'priority'    => 10,
	'transport'   => 'postMessage',
	'choices'     => array(
		'on'  => esc_attr__( 'Yes', 'textdomain' ),
		'off' => esc_attr__( 'No', 'textdomain' ),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_api_key',
	'label'    => __( 'MailChimp API Key', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 20,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_list_id',
	'label'    => __( 'MailChimp List ID', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 30,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_title',
	'label'    => __( 'Title', 'textdomain' ),
	'default'  => __( 'Newsletter', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 40,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'mc_title_color',
	'label'       => __( 'Title Color', 'textdomain' ),
	'section'     => 'church_app_mailchimp',
	'default'     => '#ffffff',
	'transport'   => 'auto',
	'priority'    => 45,
	'choices'     => array(
		'alpha' => true,
	),
	'output'      => array(
		array(
			'element' => '.hero.is-dark.mailchimp-box .title',
			'property' => 'color'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_subtitle',
	'label'    => __( 'Sub-Title', 'textdomain' ),
	'default'  => __( 'Subscribe to our newsletter today', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 50,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'mc_subtitle_color',
	'label'       => __( 'Subtitle Color', 'textdomain' ),
	'section'     => 'church_app_mailchimp',
	'default'     => '#ffffff',
	'transport'   => 'auto',
	'priority'    => 55,
	'choices'     => array(
		'alpha' => true,
	),
	'output'      => array(
		array(
			'element' => '.hero.is-dark.mailchimp-box .subtitle',
			'property' => 'color'
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_button_text',
	'label'    => __( 'Button Text', 'textdomain' ),
	'default'  => esc_attr__( 'Subscribe', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 60,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_placeholder',
	'label'    => __( 'Placeholder', 'textdomain' ),
	'default'  => esc_attr__( 'Your Email Address', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 70,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'mc_confirmation',
	'label'    => __( 'Confirmation Message', 'textdomain' ),
	'default'  => esc_attr__( 'Thank you for subscribing.', 'textdomain' ),
	'section'  => 'church_app_mailchimp',
	'transport'=> 'postMessage',
	'priority' => 80,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'background',
	'settings'    => 'mc_background',
	'label'       => esc_attr__( 'Background', 'textdomain' ),
	'section'     => 'church_app_mailchimp',
	'transport'   => 'auto',
	'priority'    => 90,
	'default'     => array(
		'background-color'      => '#363636',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'output'      => array(
		array(
			'element' => '.hero.is-dark.mailchimp-box',
		),
	),
) );


// Notification Bar

Kirki::add_section( 'church_app_notification_bar', array(
    'title'          => esc_attr__( 'Notification Bar', 'textdomain' ),
	'description'    => esc_attr__( 'Options for the bar above the site header.', 'textdomain' ),
    'priority'       => 70,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'toggle',
	'settings'    => 'notification_enable',
	'label'       => esc_attr__( 'Include Notification Bar?', 'textdomain' ),
	'section'     => 'church_app_notification_bar',
	'description' => esc_attr__( 'Enabling/disabling requires publishing before changes can be seen in the customizer.', 'textdomain' ),
	'default'     => '0',
	'transport'	  => 'postMessage',
	'priority'    => 10,
	'choices'     => array(
		'on'  => esc_attr__( 'Yes', 'textdomain' ),
		'off' => esc_attr__( 'No', 'textdomain' ),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'notification_content',
	'label'    => __( 'Content', 'textdomain' ),
	'section'  => 'church_app_notification_bar',
	'transport'=> 'postMessage',
	'priority' => 20,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'notification_color',
	'label'       => __( 'Text Color', 'textdomain' ),
	'section'     => 'church_app_notification_bar',
	'default'     => '#ffffff',
	'priority'    => 30,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.header-bar',
			'property' => 'color',
		),
		array(
			'element'  => '.header-bar',
			'property' => 'stroke',
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'notification_background',
	'label'       => __( 'Background Color', 'textdomain' ),
	'section'     => 'church_app_notification_bar',
	'default'     => 'rgba(30, 115, 190, 1)',
	'priority'    => 40,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.header-bar',
			'property' => 'background-color',
		),
	),
) );


// footer
Kirki::add_section( 'church_app_footer', array(
    'title'          => esc_attr__( 'Footer', 'textdomain' ),
    'priority'       => 80,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'footer_content',
	'label'    => __( 'Content', 'textdomain' ),
	'default'  => __( '&copy; 2018 WP Web Apps', 'textdomain' ),
	'section'  => 'church_app_footer',
	'transport' => 'postMessage',
	'priority' => 10,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'footer_color',
	'label'       => __( 'Text Color', 'textdomain' ),
	'section'     => 'church_app_footer',
	'default'     => '#121212',
	'priority'    => 20,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.footer',
			'property' => 'color',
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'background',
	'settings'    => 'footer_background',
	'label'       => esc_attr__( 'Background', 'textdomain' ),
	'section'     => 'church_app_footer',
	'priority'    => 30,
	'transport'   => 'auto',
	'default'     => array(
		'background-color'      => '#dbdbdb',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'output' => array(
		array(
			'element'  => '.footer',
		),
	),
) );

// audio player
Kirki::add_section( 'church_app_audio_player', array(
    'title'          => esc_attr__( 'Audio Player', 'textdomain' ),
    'priority'       => 90,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'background',
	'settings'    => 'player_background',
	'label'       => esc_attr__( 'Background', 'textdomain' ),
	'section'     => 'church_app_audio_player',
	'transport'   => 'auto',
	'priority'    => 10,
	'default'     => array(
		'background-color'      => 'rgba(255,255,255,.9)',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'output'      => array(
		array(
			'element' => '.audio-player',
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'audio_progress_background',
	'label'       => __( 'Progress Bar Background', 'textdomain' ),
	'section'     => 'church_app_audio_player',
	'default'     => '#242424',
	'priority'    => 20,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.playback-scale',
			'property' => 'background-color',
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'audio_progress_indicator',
	'label'       => __( 'Progress Bar Indicator', 'textdomain' ),
	'section'     => 'church_app_audio_player',
	'default'     => '#209cee',
	'priority'    => 30,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.audio-player .playback-time-indicator',
			'property' => 'background-color',
		),
	),
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'color',
	'settings'    => 'audio_icons',
	'label'       => __( 'Player Icons', 'textdomain' ),
	'section'     => 'church_app_audio_player',
	'default'     => '#000000',
	'priority'    => 30,
	'transport'   => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
		array(
			'element'  => '.vue-sound-wrapper svg path',
			'property' => 'fill',
		),
		array(
			'element'  => '.audio-player .delete',
			'property' => 'background-color',
		),
	),
) );

// Advanced
Kirki::add_section( 'church_app_advanced', array(
    'title'          => esc_attr__( 'Advanced', 'textdomain' ),
    'priority'       => 100,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'     => 'text',
	'settings' => 'ga_id',
	'label'    => __( 'Google Analytics ID', 'textdomain' ),
	'default'  => __( 'UA-XXXXXX-ID', 'textdomain' ),
	'section'  => 'church_app_advanced',
	'transport' => 'auto',
	'priority' => 10,
) );

Kirki::add_field( 'church_app_theme', array(
	'type'        => 'select',
	'settings'    => 'church_app_animation_style',
	'label'       => __( 'Page Transition', 'textdomain' ),
	'section'     => 'church_app_advanced',
	'default'     => 'fade',
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => array(
		'fade' => esc_attr__( 'Fade', 'textdomain' ),
		'slide' => esc_attr__( 'Slide', 'textdomain' ),
		'none' => esc_attr__( 'None', 'textdomain' ),
		'custom' => esc_attr__( 'Custom', 'textdomain' ),
	),
) );


function church_app_theme_customizer_live_preview() {
	wp_enqueue_script( 
		'church-app-theme-preview',
		get_template_directory_uri().'/includes/customizer-preview.js',
		array( 'jquery','customize-preview' ),
		'', 
		true
	);
}
add_action( 'customize_preview_init', 'church_app_theme_customizer_live_preview' );