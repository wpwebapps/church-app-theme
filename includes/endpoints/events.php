<?php

// add nice dates/times to events and yoast bits 
    function church_app_tribe_nice_date_time_single( array $event_data ) {
        $event_id = $event_data['id'];
    
        $yoast_keyword = get_post_meta($event_id, '_yoast_wpseo_focuskw', true);
        $seo_title = get_post_meta($event_id, '_yoast_wpseo_title', true);

        if ( $seo_title != '' ) {
            $title = $seo_title;
        } else {
            $title = get_the_title($event_id);
        }

        $event_data['keyword'] = $yoast_keyword;
        $event_data['seo_title'] = $title;
        $event_data['description'] = $description;

        $date_format = get_option('date_format');
        $time_format = get_option('time_format');

        $start_day = tribe_get_start_date( $event_id, false, $date_format );
        $start_time = tribe_get_start_date( $event_id, false, $time_format );

        $nice_start = array(
            'day'   => $start_day,
            'time'  => $start_time
        );

        $end_day = tribe_get_end_date( $event_id, false, $date_format );
        $end_time = tribe_get_end_date( $event_id, false, $time_format );

        $nice_end = array(
            'day'   => $end_day,
            'time'  => $end_time
        );

        $event_data['nice_start'] = $nice_start;
        $event_data['nice_end'] = $nice_end;


        return $event_data;
        
    }
    add_filter( 'tribe_rest_event_data', 'church_app_tribe_nice_date_time_single' );
