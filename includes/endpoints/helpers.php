<?php 

// Allow anonymous comments 
function church_core_app_rest_allow_anonymous_comments() {
    return true;
}
add_filter('rest_allow_anonymous_comments', 'church_core_app_rest_allow_anonymous_comments');


// add featured image url to page and post json
function church_core_app_add_featured_image_url_page( $data, $post, $request ) {
    
    $_data = $data->data;
    $thumbnail_id = get_post_thumbnail_id( $post->ID );
    $thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'full' );
    
    $_data['featured_image_url'] = $thumbnail[0];
    $_data['nice_date'] = get_the_date();
    $_data['description'] =  wp_trim_words(get_the_excerpt(), 20, '...');
    
    $data->data = $_data;
    return $data;

}
add_filter( 'rest_prepare_page', 'church_core_app_add_featured_image_url_page', 10, 3 );
add_filter( 'rest_prepare_post', 'church_core_app_add_featured_image_url_page', 10, 3 );


// add category name to post response
function church_core_app_add_category_name_posts( $data, $post, $request ) {
    
    $_data = $data->data;

    $categories = wp_get_post_terms( get_the_ID(), 'category' );
    $tags = wp_get_post_terms( get_the_ID(), 'tags' );

    $_data['category_list'] = $categories;
    $_data['tag_list'] = $tags;

    $post_author_id = get_post_field( 'post_author', $post_id );
    $_data['author_display_name'] = get_author_name($post_author_id);

    $data->data = $_data;
    return $data;

}
add_filter( 'rest_prepare_post', 'church_core_app_add_category_name_posts', 10, 3 );


// add nice date to pages, posts, comments
function church_core_app_comment_nice_date( $data, $post, $request ) {
    
    $_data = $data->data;
    
    $_data['nice_date'] = get_comment_date('', $post);
    
    $data->data = $_data;
    return $data;

}
add_filter( 'rest_prepare_comment', 'church_core_app_comment_nice_date', 10, 3 );


// add seo data to response 
function church_core_yoast_data( $data, $post, $request ) {

    $_data = $data->data;

    $yoast_keyword = get_post_meta($post->ID, '_yoast_wpseo_focuskw', true);
    $seo_title = get_post_meta($post->ID, '_yoast_wpseo_title', true);
    $seo_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);

    if ( $seo_description != '' ) {
        $description = $seo_description;
    } else {
        $description = strip_tags( get_the_excerpt() );
    }

    if ( $seo_title != '' ) {
        $title = $seo_title;
    } else {
        $title = get_the_title();
    }

    $_data['keyword'] = $yoast_keyword;
    $_data['seo_title'] = $title;
    $_data['description'] = $description;

    $data->data = $_data;
    return $data;

}
add_filter( 'rest_prepare_post', 'church_core_yoast_data', 10, 3 );
add_filter( 'rest_prepare_page', 'church_core_yoast_data', 10, 3 );
add_filter( 'rest_prepare_podcast', 'church_core_yoast_data', 10, 3 );
