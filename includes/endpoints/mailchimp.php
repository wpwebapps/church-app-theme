<?php


// register route for form posting
    function church_app_mailchimp_form_route() {
        register_rest_route(
            'church-app/v1',
            '/subscribe/',
            array(
                'methods'  => WP_REST_Server::CREATABLE,
                'callback' => 'church_app_mailChimp_handler',
                )
        );
    }
    add_action( 'rest_api_init', 'church_app_mailchimp_form_route' );


// MailChimp SignUp
    include_once get_template_directory() . '/includes/mailchimp.php';

    use \DrewM\MailChimp\MailChimp;

    function church_app_mailChimp_handler( $response ) {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $chimp_fields = $mods = get_theme_mods();
        $chimp_api = $chimp_fields['mc_api_key'];
        $list_id = $chimp_fields['mc_list_id'];

        $MailChimp = new MailChimp($chimp_api);

        $email = $request->email;
        
        $result = $MailChimp->post("lists/$list_id/members", [
            'email_address' => $email,
            'status'        => 'subscribed',
        ]);
        
        $data['message'] = $result;
        return $data;
      
    }