<?php 
/*
 * Create Route For Plugin Options
 *
 * Source URL: https://webdevstudios.com/2016/05/24/wp-api-adding-custom-endpoints/
 *
 * Endpoint URL: http://site.com/wp-json/church-app/v1/options
 *
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit; // Exit if accessed directly.
 }

function church_app_options_register_route() {
	register_rest_route( 'church-app/v1', 'options', array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'church_app_options_serve_route',
		) );
}
add_action( 'rest_api_init', 'church_app_options_register_route' );


function church_app_options_serve_route( WP_REST_Request $request ) {

    $site_title = get_bloginfo('name');
    $template_url = get_template_directory_uri();

    $mods = get_theme_mods();

    // branding
    if ( $mods['header_logo'] == null ) {
        $header_logo = get_template_directory_uri().'/images/redeemer-church.png';
    } else {
        $header_logo = $mods['header_logo'];
    }

    if ( $mods['footer_logo'] == null ) {
        $footer_logo = get_template_directory_uri().'/images/redeemer-church.png';
    } else {
        $footer_logo = $mods['footer_logo'];
    }

    $branding = array(
        'header_logo' => $header_logo,
        'footer_logo'=> $footer_logo
    );

    // footer
    if ( $mods['footer_content'] == null ) {
        $footer_text = '&copy; 2018 WP Web Apps';
    } else {
        $footer_text = $mods['footer_content'];
    }
    
    $footer = array(
        'footer_text' => $footer_text
    );

    // mailchimp

    if ( $mods['mc_include'] == null ) {
        $mc_include = false;
    } else {
       $mc_include = $mods['mc_include'];
    }

    if ( $mods['mc_title'] == null ) {
        $intro_title = 'Newsletter';
    } else {
        $intro_title = $mods['mc_title'];
    }

    if ( $mods['intro_subtitle'] == null ) {
        $intro_subtitle = 'Subscribe to our newsletter today';
    } else {
        $intro_subtitle = $mods['intro_subtitle'];
    }

    if ( $mods['mc_button_text'] == null ) {
        $button_text = 'Subscribe';
    } else {
        $button_text = $mods['mc_button_text'];
    }

    if ( $mods['mc_placeholder'] == null ) {
        $placeholder = 'Your Email Address';
    } else {
        $placeholder = $mods['mc_placeholder'];
    }

    if ( $mods['mc_confirmation'] == null ) {
        $confirmation = 'Thank you for subscribing';
    } else {
        $confirmation = $mods['mc_confirmation'];
    }

    $mailchimp_array = array(
        'include'       => $mc_include,
        'intro_title'   => $intro_title,
        'intro_subtitle'=> $intro_subtitle,
        'button_text'   => $button_text,
        'placeholder'   => $placeholder,
        'confirmation'  => $confirmation
    );

    
    // notification bar
    if ( $mods['notification_enable'] == null ) {
        $notification_enable = false;
    } else {
        $notification_enable = $mods['notification_enable'];
    }

    if ( $mods['notification_content'] == null ) {
        $notification_content = '';
    } else {
        $notification_content = $mods['notification_content'];
    }

    if ( $mods['notification_search'] == null ) {
        $notification_search = false;
    } else {
        $notification_search = $mods['notification_search'];
    }

    $header = array(
        'display_bar' => $notification_enable,
        'content' => $notification_content,
        'search_include' => $notification_search,
    );

    // nav menu
    // from https://developer.wordpress.org/reference/functions/wp_get_nav_menu_items/

    $menu_name = 'app-main';
    $locations = get_nav_menu_locations();
    $menu_id = $locations[ $menu_name ];
    $wp_menu = wp_get_nav_menu_object( $menu_id );
    
    $menu_items = wp_get_nav_menu_items( $wp_menu->term_id );

			/**
			 * wp_get_nav_menu_items() outputs a list that's already sequenced correctly.
			 * So the easiest thing to do is to reverse the list and then build our tree
			 * from the ground up
			 */
			$rev_items = array_reverse ( $menu_items );
			$rev_menu  = array();
			$cache     = array();

			foreach ( $rev_items as $item ) :

				$formatted = array(
					'ID'          => abs( $item->ID ),
					'order'       => (int) $item->menu_order,
					'parent'      => abs( $item->menu_item_parent ),
                    'title'       => $item->title,
                    'slug'        => basename( $item->url ),
					'url'         => $item->url,
					'target'      => $item->target,
					'children'    => array(),
				);

				if ( array_key_exists( $item->ID , $cache ) ) {
					$formatted['children'] = array_reverse( $cache[ $item->ID ] );
				}

            	$formatted = apply_filters( 'rest_menus_format_menu_item', $formatted );

				if ( $item->menu_item_parent != 0 ) {

					if ( array_key_exists( $item->menu_item_parent , $cache ) ) {
						array_push( $cache[ $item->menu_item_parent ], $formatted );
					} else {
						$cache[ $item->menu_item_parent ] = array( $formatted, );
					}

				} else {

					array_push( $rev_menu, $formatted );
				}

			endforeach;

            $menu = array_reverse ( $rev_menu );
           
    // transitions

    if ( $mods['church_app_animation_style'] == null ) {
        $church_app_animation_style = 'fade';
    } else {
        $church_app_animation_style = $mods['church_app_animation_style'];
    }

    $transitions = array(
        'type'    => $church_app_animation_style
    );

    // send completed options array
    $options_array = array(
        'template_url' => $template_url,
        'site_title' => $site_title,
        'branding' => $branding,
        'footer' => $footer,
        'mailchimp' => $mailchimp_array,
        'header' => $header,
        'transition' => $transitions,
        'menu' => $menu
	);

	wp_send_json( $options_array );

}