<?php

// add page builder to API response
    function church_core_app_layout_builder( $data, $post, $request ) {
            
        $_data = $data->data;

        $hero = get_field( 'hero' );
        $builder = get_field( 'include_builder' );
        $include_hero = get_field( 'hero_include' );
        $include_form = get_field( 'include_form' );
        $form_id = get_field( 'form_id' );
        $rows = get_field( 'rows' );

        $_data['hero'] = $hero;
        $_data['builder'] = $builder;
        $_data['include_hero'] = $include_hero;
        $_data['form'] = $include_form;
        $_data['form_id'] = $form_id;
        $_data['rows'] = $rows;
        
        $data->data = $_data;
        return $data;

    }
    add_filter( 'rest_prepare_page', 'church_core_app_layout_builder', 10, 3 );