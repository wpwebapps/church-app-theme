<?php

// Register post type for form entries
    function church_app_form_type_register() {
            
        if ( post_type_exists( 'wpwa-form' ) ) {
            return;
        }
        
        $wpwa_form_args = array(
            'label'                 => __( 'Forms', 'church_core' ),
            'description'           => __( 'Contact form submissions', 'church_core' ),
            'supports'              => array( 'title', 'custom-fields' ),
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-welcome-write-blog',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => true,		
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
            'labels' 				=> array(
                'name'                  => _x( 'Entries', 'church_core' ),
                'singular_name'         => _x( 'Entry', 'church_core' ),
                'menu_name'             => __( 'Forms', 'church_core' ),
                'name_admin_bar'        => __( 'Entries', 'church_core' ),
                'archives'              => __( 'Entries', 'church_core' ),
                'all_items'             => __( 'Entries', 'church_core' ),
                'add_new_item'          => __( 'Add New', 'church_core' ),
                'add_new'               => __( 'Add Entries', 'church_core' ),
                'new_item'              => __( 'New Entry', 'church_core' ),
                'edit_item'             => __( 'Edit Entry', 'church_core' ),
                'update_item'           => __( 'Update Entry', 'church_core' ),
                'view_item'             => __( 'View Entry', 'church_core' ),
                'search_items'          => __( 'Search Entries', 'church_core' ),
                'not_found'             => __( 'No entries', 'church_core' ),
                'not_found_in_trash'    => __( 'No entries in the trash', 'church_core' ),
            ),
        );
        
        register_post_type( 'wpwa-form', apply_filters( 'wpwa_form_type_params', $wpwa_form_args ) );

    }
    add_action( 'init', 'church_app_form_type_register', 0 );


// register form settings post type
    function church_core_form_fields_register() {

        $labels = array(
            'name'                  => _x( 'Forms', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Form', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Forms', 'text_domain' ),
            'name_admin_bar'        => __( 'Forms', 'text_domain' ),
            'archives'              => __( 'Forms', 'text_domain' ),
            'attributes'            => __( 'Form Attributes', 'text_domain' ),
            'parent_item_colon'     => __( 'Parent Form', 'text_domain' ),
            'all_items'             => __( 'Forms', 'text_domain' ),
            'add_new_item'          => __( 'Add New Form', 'text_domain' ),
            'add_new'               => __( 'Add Form', 'text_domain' ),
            'new_item'              => __( 'New Form', 'text_domain' ),
            'edit_item'             => __( 'Edit Form', 'text_domain' ),
            'update_item'           => __( 'Update Form', 'text_domain' ),
            'view_item'             => __( 'View Form', 'text_domain' ),
            'view_items'            => __( 'View Forms', 'text_domain' ),
            'search_items'          => __( 'Search Form', 'text_domain' ),
            'not_found'             => __( 'Not found', 'text_domain' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
            'featured_image'        => __( 'Featured Image', 'text_domain' ),
            'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
            'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
            'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
            'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
            'items_list'            => __( 'Items list', 'text_domain' ),
            'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
            'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
        );
        $args = array(
            'label'                 => __( 'Form', 'text_domain' ),
            'description'           => __( 'Fields for our custom form', 'text_domain' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'custom-fields' ),
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => 'edit.php?post_type=wpwa-form',
            'menu_position'         => 5,
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );
        register_post_type( 'wpwa-form-fields', $args );

    }
    add_action( 'init', 'church_core_form_fields_register', 0 );


// add page builder to API response
    function church_app_form_fields_route_data( $data, $post, $request ) {
        
        // remove default data
        $_data = '';
        $data->remove_link( 'collection' );
        $data->remove_link( 'self' );
        $data->remove_link( 'about' );
        $data->remove_link( 'author' );
        $data->remove_link( 'replies' );
        $data->remove_link( 'version-history' );
        $data->remove_link( 'https://api.w.org/featuredmedia' );
        $data->remove_link( 'https://api.w.org/attachment' );
        $data->remove_link( 'https://api.w.org/term' );
        $data->remove_link( 'curies' );

        $raw_fields = get_field( 'fields' );

        foreach ( (array) $raw_fields as $key => $field ) {

            if ( $field['type'] == 'input') {
                $inputType = 'text';
            } else {
                $inputType = '';
            }

            if ( is_array( $ $field['options'] ) ) {
                $options = array();
                foreach ( $field['options'] as $option ) {
                    $options[] = implode( " ", $option);
                }
            } else {
                $options = '';
            }

            $fields[] = array(
                'model' => $field['name'],
                'type' => $field['type'],
                'inputType' => $inputType,
                'required' => $field['required'],
                'label' => $field['label'],
                'hint' => $field['description'],
                'rows' => 6,
                'values' => $options
            );

            $field['name'] = $field['type'] = $field['inputType'] = $field['required'] = $field['label'] = $field['hint'] = $field['values'] = '';
        
        }

        $_data['id'] = get_the_id();
        $_data['title'] = get_the_title();
        $_data['description'] = get_field( 'description' );
        $_data['button'] =  get_field( 'button' );
        $_data['confirmation'] = get_field( 'confirmation' );
        $_data['fields'] = $fields;
        
        $data->data = $_data;
        return $data;

    }
    add_filter( 'rest_prepare_wpwa-form-fields', 'church_app_form_fields_route_data', 10, 3 );


// register route for form posting
    function church_app_forms_register_route() {
        register_rest_route('church-app/v1','/form/', array(
                'methods'  => WP_REST_Server::CREATABLE,
                'callback' => 'church_app_form_post_handler',
            )
        );
    }
    add_action( 'rest_api_init', 'church_app_forms_register_route' );


// create form post from response	
    function church_app_form_post_handler( $response ) {

        $postdata = file_get_contents( "php://input" );
        $request = json_decode( $postdata );
        
        $content = $request->content;

        $name = $content->name;

        $entry_title = $name;
        
        $id = wp_insert_post(
            array(
                'post_type' => 'wpwa-form', 
                'post_status' => 'publish',
                'post_title' => $entry_title,
                'meta_input' =>  $content
            )
        );

        $result = wp_send_json( $content );
   
        return $result;
        
    }



// Send form entries via email
	function church_app_contact_form_notification( $new_status, $old_status, $post )  {
	
	    if ( 'publish' !== $new_status or 'publish' === $old_status
	        or 'wpwa-form' !== get_post_type( $post ) )
	        return;
	    
	    $current_post = get_post( $post ); 
	    $postid = $current_post->ID;
	    
	    $admin_email = get_option( 'admin_email' );
	    $subject = 'New Contact From ' .$title;
	   	$link = get_permalink( $post );
	   	$headers[]   = 'Reply-To: '.$title.' <'.$email_address.'>';
        $custom = get_post_custom( $postid );
	    $body = $custom. '<br />'. $link;

		wp_mail( $admin_email, $subject, $body, $headers );
	    
	}
	add_action( 'transition_post_status', 'church_app_contact_form_notification', 10, 3 );


// remove add entry and add submenu to create new form to admin    
    function church_app_contact_form_menu_mods() {
        remove_submenu_page( 'edit.php?post_type=wpwa-form', 'post-new.php?post_type=wpwa-form' );
        add_submenu_page(
            'edit.php?post_type=wpwa-form',
            __( 'Add Form', 'text-domain' ),
            __( 'Add Form', 'text-domain' ),
            'manage_options',
            'post-new.php?post_type=wpwa-form-fields'
        );
    }
    add_action( 'admin_menu', 'church_app_contact_form_menu_mods' );


// create metabox to show all entered fields
function church_app_form_entry_add_metaboxes( $post ) {
    add_meta_box( 
        'church_app_form_entry_metabox',
        __( 'Form Content', 'text-domain' ),
        'church_app_display_form_content',
        'wpwa-form',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes', 'church_app_form_entry_add_metaboxes' );

function church_app_display_form_content() {
    $custom = get_post_custom();
    echo '<table>';
    foreach($custom as $key => $value) {
        $value = implode( " ", $value);
        echo '
        <tr>
            <td><strong style="text-transform: capitalize">'.$key.'</strong></td>
            <td>'.$value.'</td>
        </tr>'
        ;
    }
    echo '</table>';
}