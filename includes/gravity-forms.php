<?php

// correct form action URL for Gravity Forms    
    function church_app_theme_gf_action_tag($form_tag, $form) {

        global $wp;
        $slug = htmlspecialchars( $_GET["slug"] );
        $actual_link = home_url( $slug );
        $form_tag = preg_replace( "|action='(.*?)'|", "action=$actual_link", $form_tag );
        return $form_tag;

    }
    add_filter( 'gform_form_tag', 'church_app_theme_gf_action_tag', 10, 2 );
