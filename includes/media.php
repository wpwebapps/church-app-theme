<?php

// add wrapper for oembed objects
    function church_app_theme_embed_wrapper($html, $url, $attr) {

        return '<div class="oembed-content">' . $html . '</div>';

    }
    add_filter('embed_oembed_html', 'church_app_theme_embed_wrapper', 99, 4);


// change attachment url to match app   
    function church_app_theme_attachment_link( $link, $post_id ){
        $post = get_post( $post_id );
        return home_url( '/media/' . $post->post_name );
    }
    add_filter( 'attachment_link', 'church_app_theme_attachment_link', 20, 2 );
