<?php


// load hashed js files from build
    if ( ! function_exists( 'church_app_theme_enqueue_hashed_js') ) {
        function church_app_theme_enqueue_hashed_js() {

            wp_deregister_script( 'comment-reply' ); 

            $dirJS = new DirectoryIterator(get_stylesheet_directory() . '/static/js');

            foreach ($dirJS as $file) {

                if (pathinfo($file, PATHINFO_EXTENSION) === 'js') {
                $fullName = basename($file);
                $name = substr(basename($fullName), 0, strpos(basename($fullName), '.'));
            
                switch($name) {
            
                    case 'app':
                        $deps = array('vendor');
                        break;
                    case 'vendor':
                        $deps = array('manifest');
                        break;
                    default:
                        $deps = null;               
                        break;
            
                }
            
                wp_enqueue_script( $name, get_template_directory_uri() . '/static/js/' . $fullName, $deps, null, true );
                
                $url = trailingslashit( get_site_url() );
                
                }
            
            }
            wp_localize_script('app', 'baseURL', array(
                'url' => $url
                // replace "http://yoursite.com" with baseURL.url
                // wpBase:"http://yoursite.com" becomes baseURL:baseURL.url
                )
            
            );

        }
    }
    add_action( 'wp_enqueue_scripts', 'church_app_theme_enqueue_hashed_js', 0 );



// add hashed css files from build
    if ( ! function_exists( 'church_app_theme_enqueue_hashed_css') ) {
        function church_app_theme_enqueue_hashed_css() {

            $dirCSS = new DirectoryIterator(get_stylesheet_directory() . '/static/css');

            foreach ($dirCSS as $file) {

                if (pathinfo($file, PATHINFO_EXTENSION) === 'css') {
                    $fullName = basename($file);
                    $name = substr(basename($fullName), 0, strpos(basename($fullName), '.'));
                
                    switch($name) {
                
                        default:
                            $deps = null;               
                            break;
                
                    }
                
                    wp_enqueue_style( $name, get_template_directory_uri() . '/static/css/' . $fullName, $deps, null );
                
                }
            
            }

        }
    }
    add_action( 'wp_enqueue_scripts', 'church_app_theme_enqueue_hashed_css', 0 );

// remove church core css 
    function church_app_theme_remove_church_core_css() {
        wp_dequeue_style( 'church-core' );
        wp_dequeue_script( 'wp-embed' );
    }
    add_action( 'wp_print_scripts', 'church_app_theme_remove_church_core_css', 1 );


// remove emoji head scripts and styles
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
    remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
    remove_action( 'admin_print_styles', 'print_emoji_styles' );


// remove The Event Calendar Styles and scripts (they need a simple filter for this)  
    function church_app_theme_remove_tribe_event_styles() {
            
        $handles_to_remove = array(
            'tribe-events-bootstrap-datepicker-css',
            'tribe-events-custom-jquery-styles',
            'tribe-events-calendar-style',
            'tribe-events-calendar-mobile-style',
            'tribe-events-full-calendar-style',
            'tribe-events-calendar-full-mobile-style',
            'tribe_events-widget-calendar-pro-style', 
            'tribe_events--widget-calendar-pro-override-style',
            'tribe-events-calendar-pro-style',
            'tribe-events-calendar-pro-mobile-style',
            'tribe-events-full-pro-calendar-style',
            'tribe-events-calendar-full-pro-mobile-style',
        );

        foreach( $handles_to_remove as $key => $handle ) {
            if( wp_style_is( $handle, $list = 'enqueued' ) ) {
                wp_dequeue_style( $handle );
            }
        }
    }
    add_action( 'wp_enqueue_scripts', 'church_app_theme_remove_tribe_event_styles', 20 );

    
    function church_app_theme_dequeue_tribe_events_scripts() {
        wp_dequeue_script( 'the-events-calendar' );
        wp_dequeue_script( 'tribe-js' );
        wp_dequeue_script( 'tribe-events-jquery-resize' );
        wp_dequeue_script( 'tribe-events-calendar-script' );
        wp_dequeue_script( 'jquery-placeholder' );
        wp_dequeue_script( 'tribe-events-bootstrap-datepicker' );
        wp_dequeue_script( 'tribe-events-bar' );
        wp_dequeue_script( 'tribe-events-list' );
        wp_dequeue_script( 'tribe-events-pro-geoloc' );
        wp_dequeue_script( 'tribe-events-pro' );
        wp_dequeue_script( 'TribeCalendar' );
    }
    add_action('wp_enqueue_scripts', 'church_app_theme_dequeue_tribe_events_scripts', 10 );