<!DOCTYPE html>
<html lang=en>

<head>
  <meta charset=utf-8>
  <meta http-equiv=X-UA-Compatible content="IE=edge">
  <meta name=viewport content="width=device-width,initial-scale=1">
  
  <?php wp_head(); ?>

</head>

<?php if ( is_admin_bar_showing() ) { ?>
  <body class="admin-bar">
<?php } else { ?>
  <body>
<?php } ?>

    <div id=app>
      <div class="app-loading" style="height: 100vh; width: 100vw; display: flex; align-items: center;justify-content: center; background-color: #f7f7f7;">
        <img src="<?php echo get_theme_mod( 'loading_logo' ); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>"/>
      </div>   
    </div>

    <?php wp_footer(); ?>
    <?php if ( get_theme_mod( 'church_app_ga' )  !='' ) { ?>
    <script>
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })
      (window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

      ga('create', '<?php echo get_theme_mod( 'church_app_ga' ); ?>', 'auto');

    </script>
    <?php } ?>

</body>

</html>